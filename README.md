# It's all connected! Have fun with Cyber Threat Intelligence 👽 
---


📕 Este repositorio contiene las pruebas de concepto para automatización de CTI a través de diversas fuentes OSINT y otras privativas. 
Además contiene una prueba de concepto de Machine Learning. 



![Presentación](./presentation/portada.png)


## Recursos 🚀

Durante la charla vimos dos pruebas de concepto. 

1. 📁 `CTI - Extract data:` Contiene Notebook con PoC de extracción de IoCs 
2. 📁 `ML - Supervisado:` Contiene Notebook con PoC de detección de archivos maliciosos según la entropía. 



### Presentación 📋

La encontrarás en la carpeta 📁 `presentation`

### Recursos externos para seguir profundizando 🛠️ 🔧

Para ampliar sobre CTI en general:
1. 📘 [Threat Threat Intelligence Handbook](https://paper.bobylive.com/Security/threat-intelligence-handbook-second-edition.pdf) 
2. 📘 [Practical Threat Intelligence and Data-Driven Threat Hunting](https://www.amazon.com/-/es/Valentina-Costa-Gazc%C3%B3n/dp/1838556370)
3. 📘 [Learning Malware Analysis](https://www.packtpub.com/product/learning-malware-analysis/9781788392501)
4. 📘 [Practical Malware Analysis](https://www.amazon.com/-/es/Michael-Sikorski/dp/1593272901)

Para Machine Learning:
1. 🤖 [Hands-On Artificial Intelligence for Cybersecurity](https://www.packtpub.com/product/hands-on-artificial-intelligence-for-cybersecurity/9781789804027)
2. 🤖 [Hands-On Machine Learning for Cybersecurity](https://www.packtpub.com/product/hands-on-machine-learning-for-cybersecurity/9781788992282)


⌨️ con ❤️ por [Sol](https://twitter.com/solg_sh) 😊